# mermaid-man

Emacs Theme inspired by Bikini Bottom's greatest hero.

# Install

Simply clone this repo, move mermaid-man-theme.el to ~/.emacs.d

Then add 
```
(load-theme 'mermaid-theme t)
```

To your .emacs file
